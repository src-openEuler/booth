From f31d896ba5f72853aed8191dbb99f9c670fcb866 Mon Sep 17 00:00:00 2001
From: Chris Lumens <clumens@redhat.com>
Date: Wed, 26 Jun 2024 14:43:56 -0400
Subject: [PATCH 14/20] Refactor: Fix problems found by clang.

* Several strings are initialized to some value, immediately followed by
  a block of code that sets them to some other value.  The original
  initialization can go away.

* Warn if vsnprintf fails.

* Get rid of setting a couple return values that are never checked.

* In read_client, set msg to 0 if we malloc'd it so that header->length
  is an initialized value when we access it later on.

* In _find_myself, check that tb[IFA_ADDRESS] is not NULL before
  accessing it.  I'm not sure what we can do if both this and
  tb[IFA_LOCAL] are NULL aside from log and return.
---
 src/alt/nametag_libsystemd.c |  4 ++++
 src/attr.c                   |  2 +-
 src/main.c                   |  3 +--
 src/ticket.c                 |  2 +-
 src/transport.c              | 11 ++++++++---
 5 files changed, 15 insertions(+), 7 deletions(-)

diff --git a/src/alt/nametag_libsystemd.c b/src/alt/nametag_libsystemd.c
index 26e5009..114b0e4 100644
--- a/src/alt/nametag_libsystemd.c
+++ b/src/alt/nametag_libsystemd.c
@@ -73,6 +73,10 @@ void sd_notify_wrapper(const char *fmt, ...)
 	rv = vsnprintf(buffer, sizeof(buffer), suffix, ap);
 	va_end(ap);
 
+	if (rv < 0) {
+		log_warn("%s:%d: vsnprintf fail", __FILE__, __LINE__);
+	}
+
 	rv = sd_notifyf(0, "READY=1\n"
 			"STATUS=Running: %s",
 			buffer);
diff --git a/src/attr.c b/src/attr.c
index bc154f0..daa7648 100644
--- a/src/attr.c
+++ b/src/attr.c
@@ -65,7 +65,7 @@ void print_geostore_usage(void)
 int test_attr_reply(cmd_result_t reply_code, cmd_request_t cmd)
 {
 	int rv = 0;
-	const char *op_str = "";
+	const char *op_str = NULL;
 
 	switch (cmd) {
 	case ATTR_SET:	op_str = "set";		break;
diff --git a/src/main.c b/src/main.c
index 71932fa..6efd569 100644
--- a/src/main.c
+++ b/src/main.c
@@ -584,7 +584,7 @@ fail:
 static int test_reply(cmd_result_t reply_code, cmd_request_t cmd)
 {
 	int rv = 0;
-	const char *op_str = "";
+	const char *op_str = NULL;
 
 	if (cmd == CMD_GRANT)
 		op_str = "grant";
@@ -743,7 +743,6 @@ static int query_get_string_answer(cmd_request_t cmd)
 	*(data + data_len) = '\0';
 	(void)fputs(data, stdout);
 	fflush(stdout);
-	rv = 0;
 
 out_test_reply:
 	rv = test_reply_f(ntohl(reply.header.result), cmd);
diff --git a/src/ticket.c b/src/ticket.c
index 622a9d1..67c7cd0 100644
--- a/src/ticket.c
+++ b/src/ticket.c
@@ -544,7 +544,7 @@ void reset_ticket_and_set_no_leader(struct ticket_config *tk)
 static void log_reacquire_reason(struct ticket_config *tk)
 {
 	int valid;
-	const char *where_granted = "\0";
+	const char *where_granted = NULL;
 	char buff[75];
 
 	valid = is_time_set(&tk->term_expires) && !is_past(&tk->term_expires);
diff --git a/src/transport.c b/src/transport.c
index 8267c96..40a46d2 100644
--- a/src/transport.c
+++ b/src/transport.c
@@ -232,9 +232,13 @@ int _find_myself(int family, struct booth_site **mep, int fuzzy_allowed)
 				if (tb[IFA_LOCAL]) {
 					memcpy(ipaddr, RTA_DATA(tb[IFA_LOCAL]),
 							BOOTH_IPADDR_LEN);
-				} else {
+				} else if (tb[IFA_ADDRESS]) {
 					memcpy(ipaddr, RTA_DATA(tb[IFA_ADDRESS]),
 							BOOTH_IPADDR_LEN);
+				} else {
+					log_error("failed to copy netlink addr");
+					close(fd);
+					return 0;
 				}
 
 				/* Try to find the exact address or the address with subnet matching.
@@ -387,6 +391,8 @@ int read_client(struct client *req_cl)
 			log_error("out of memory for client messages");
 			return -1;
 		}
+
+		memset(msg, 0, MAX_MSG_LEN);
 		req_cl->msg = (void *)msg;
 	} else {
 		msg = (char *)req_cl->msg;
@@ -616,8 +622,7 @@ static int connect_nonb(int sockfd, const struct sockaddr *saptr,
 	tval.tv_sec = sec;
 	tval.tv_usec = 0;
 
-	if ((n = select(sockfd + 1, &rset, &wset, NULL,
-	    sec ? &tval : NULL)) == 0) {
+	if (select(sockfd + 1, &rset, &wset, NULL, sec ? &tval : NULL) == 0) {
 		/* leave outside function to close */
 		/* timeout */
 		/* close(sockfd); */	
-- 
2.25.1

