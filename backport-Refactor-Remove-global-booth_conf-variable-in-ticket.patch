From c237b423ac3dac1b51eaeed2126e0c8cd5d7faca Mon Sep 17 00:00:00 2001
From: Chris Lumens <clumens@redhat.com>
Date: Tue, 3 Sep 2024 16:00:46 -0400
Subject: [PATCH 49/50] Refactor: Remove global booth_conf variable in
 ticket.c.

Additionally, clean up a little bit of coding style by adding braces
around conditional blocks and make list_ticket a static function.

Co-authored-by: Jan Pokorny <jpokorny@redhat.com>
---
 src/main.c      |  4 +--
 src/ticket.c    | 79 +++++++++++++++++++++++++++----------------------
 src/ticket.h    | 30 ++++++++++++++++---
 src/transport.c |  2 +-
 4 files changed, 72 insertions(+), 43 deletions(-)

diff --git a/src/main.c b/src/main.c
index 4435466..7d93296 100644
--- a/src/main.c
+++ b/src/main.c
@@ -507,7 +507,7 @@ static int process_signals(void)
 	}
 	if (sig_usr1_handler_called) {
 		sig_usr1_handler_called = 0;
-		tickets_log_info();
+		tickets_log_info(booth_conf);
 	}
 	if (sig_chld_handler_called) {
 		sig_chld_handler_called = 0;
@@ -570,7 +570,7 @@ static int loop(int fd)
 			}
 		}
 
-		process_tickets();
+		process_tickets(booth_conf);
 
 		if (process_signals() != 0) {
 			return 0;
diff --git a/src/ticket.c b/src/ticket.c
index 263fb86..26b43f1 100644
--- a/src/ticket.c
+++ b/src/ticket.c
@@ -382,12 +382,13 @@ int do_revoke_ticket(struct ticket_config *tk)
 	}
 }
 
-static int number_sites_marked_as_granted(struct ticket_config *tk)
+static int number_sites_marked_as_granted(struct booth_config *conf,
+					  struct ticket_config *tk)
 {
 	int i, result = 0;
 	struct booth_site *ignored __attribute__((unused));
 
-	_FOREACH_NODE(i, ignored) {
+	FOREACH_NODE(conf, i, ignored) {
 		result += tk->sites_where_granted[i];
 	}
 
@@ -395,7 +396,7 @@ static int number_sites_marked_as_granted(struct ticket_config *tk)
 }
 
 
-int list_ticket(char **pdata, unsigned int *len)
+static int list_ticket(struct booth_config *conf, char **pdata, unsigned int *len)
 {
 	struct ticket_config *tk;
 	struct booth_site *site;
@@ -410,10 +411,10 @@ int list_ticket(char **pdata, unsigned int *len)
 	*pdata = NULL;
 	*len = 0;
 
-	alloc = booth_conf->ticket_count * (BOOTH_NAME_LEN * 2 + 128 + 16);
+	alloc = conf->ticket_count * (BOOTH_NAME_LEN * 2 + 128 + 16);
 
-	_FOREACH_TICKET(i, tk) {
-		multiple_grant_warning_length = number_sites_marked_as_granted(tk);
+	FOREACH_TICKET(conf, i, tk) {
+		multiple_grant_warning_length = number_sites_marked_as_granted(conf, tk);
 
 		if (multiple_grant_warning_length > 1) {
 			// 164: 55 + 45 + 2*number_of_multiple_sites + some margin
@@ -422,18 +423,20 @@ int list_ticket(char **pdata, unsigned int *len)
 	}
 
 	data = malloc(alloc);
-	if (!data)
+	if (!data) {
 		return -ENOMEM;
+	}
 
 	cp = data;
-	_FOREACH_TICKET(i, tk) {
+	FOREACH_TICKET(conf, i, tk) {
 		if ((!is_manual(tk)) && is_time_set(&tk->term_expires)) {
 			/* Manual tickets doesn't have term_expires defined */
 			ts = wall_ts(&tk->term_expires);
 			strftime(timeout_str, sizeof(timeout_str), "%F %T",
 					localtime(&ts));
-		} else
+		} else {
 			strcpy(timeout_str, "INF");
+		}
 
 		if (tk->leader == local && is_time_set(&tk->delay_commit)
 				&& !is_past(&tk->delay_commit)) {
@@ -443,8 +446,9 @@ int list_ticket(char **pdata, unsigned int *len)
 					sizeof(pending_str) - strlen(" (commit pending until ") - 1,
 					"%F %T", localtime(&ts));
 			strcat(pending_str, ")");
-		} else
+		} else {
 			*pending_str = '\0';
+		}
 
 		cp += snprintf(cp,
 				alloc - (cp - data),
@@ -474,8 +478,8 @@ int list_ticket(char **pdata, unsigned int *len)
 		}
 	}
 
-	_FOREACH_TICKET(i, tk) {
-		multiple_grant_warning_length = number_sites_marked_as_granted(tk);
+	FOREACH_TICKET(conf, i, tk) {
+		multiple_grant_warning_length = number_sites_marked_as_granted(conf, tk);
 
 		if (multiple_grant_warning_length > 1) {
 			cp += snprintf(cp,
@@ -483,7 +487,7 @@ int list_ticket(char **pdata, unsigned int *len)
 					"\nWARNING: The ticket %s is granted to multiple sites: ",  // ~55 characters
 					tk->name);
 
-			_FOREACH_NODE(site_index, site) {
+			FOREACH_NODE(conf, site_index, site) {
 				if (tk->sites_where_granted[site_index] > 0) {
 					cp += snprintf(cp,
 						alloc - (cp - data),
@@ -660,14 +664,14 @@ int setup_ticket(struct booth_config *conf)
 }
 
 
-int ticket_answer_list(int fd)
+int ticket_answer_list(struct booth_config *conf, int fd)
 {
 	char *data;
 	int rv;
 	unsigned int olen;
 	struct boothc_hdr_msg hdr;
 
-	rv = list_ticket(&data, &olen);
+	rv = list_ticket(conf, &data, &olen);
 	if (rv < 0)
 		goto out;
 
@@ -857,18 +861,19 @@ int leader_update_ticket(struct ticket_config *tk)
 }
 
 
-static void log_lost_servers(struct ticket_config *tk)
+static void log_lost_servers(struct booth_config *conf, struct ticket_config *tk)
 {
 	struct booth_site *n;
 	int i;
 
-	if (tk->retry_number > 1)
+	if (tk->retry_number > 1) {
 		/* log those that we couldn't reach, but do
 		 * that only on the first retry
 		 */
 		return;
+	}
 
-	_FOREACH_NODE(i, n) {
+	FOREACH_NODE(conf, i, n) {
 		if (!(tk->acks_received & n->bitmask)) {
 			tk_log_warn("%s %s didn't acknowledge our %s, "
 			"will retry %d times",
@@ -880,7 +885,7 @@ static void log_lost_servers(struct ticket_config *tk)
 	}
 }
 
-static void resend_msg(struct ticket_config *tk)
+static void resend_msg(struct booth_config *conf, struct ticket_config *tk)
 {
 	struct booth_site *n;
 	int i;
@@ -888,7 +893,7 @@ static void resend_msg(struct ticket_config *tk)
 	if (!(tk->acks_received ^ local->bitmask)) {
 		ticket_broadcast(tk, tk->last_request, 0, RLT_SUCCESS, 0);
 	} else {
-		_FOREACH_NODE(i, n) {
+		FOREACH_NODE(conf, i, n) {
 			if (!(tk->acks_received & n->bitmask)) {
 				n->resend_cnt++;
 				tk_log_debug("resending %s to %s",
@@ -902,7 +907,7 @@ static void resend_msg(struct ticket_config *tk)
 	}
 }
 
-static void handle_resends(struct ticket_config *tk)
+static void handle_resends(struct booth_config *conf, struct ticket_config *tk)
 {
 	int ack_cnt;
 
@@ -934,11 +939,11 @@ static void handle_resends(struct ticket_config *tk)
 			ack_cnt);
 		}
 	} else {
-		log_lost_servers(tk);
+		log_lost_servers(conf, tk);
 	}
 
 just_resend:
-	resend_msg(tk);
+	resend_msg(conf, tk);
 }
 
 int postpone_ticket_processing(struct ticket_config *tk)
@@ -1014,7 +1019,7 @@ static void ticket_lost(struct ticket_config *tk)
 	}
 }
 
-static void next_action(struct ticket_config *tk)
+static void next_action(struct booth_config *conf, struct ticket_config *tk)
 {
 	int rv;
 
@@ -1031,7 +1036,7 @@ static void next_action(struct ticket_config *tk)
 			}
 		} else {
 			if (tk->acks_expected) {
-				handle_resends(tk);
+				handle_resends(conf, tk);
 			}
 		}
 		break;
@@ -1068,7 +1073,7 @@ static void next_action(struct ticket_config *tk)
 			} else {
 				/* Otherwise, just send ACKs if needed */
 				if (tk->acks_expected) {
-					handle_resends(tk);
+					handle_resends(conf, tk);
 				}
 			}
 		}
@@ -1082,7 +1087,7 @@ static void next_action(struct ticket_config *tk)
 	case ST_LEADER:
 		/* timeout or ticket renewal? */
 		if (tk->acks_expected) {
-			handle_resends(tk);
+			handle_resends(conf, tk);
 			if (majority_of_bits(tk, tk->acks_received)) {
 				leader_update_ticket(tk);
 			}
@@ -1100,7 +1105,7 @@ static void next_action(struct ticket_config *tk)
 	}
 }
 
-static void ticket_cron(struct ticket_config *tk)
+static void ticket_cron(struct booth_config *conf, struct ticket_config *tk)
 {
 	/* don't process the tickets too early after start */
 	if (postpone_ticket_processing(tk)) {
@@ -1137,30 +1142,32 @@ static void ticket_cron(struct ticket_config *tk)
 		goto out;
 	}
 
-	next_action(tk);
+	next_action(conf, tk);
 
 out:
 	tk->next_state = 0;
-	if (!tk->in_election && tk->update_cib)
+	if (!tk->in_election && tk->update_cib) {
 		ticket_write(tk);
+	}
 }
 
 
-void process_tickets(void)
+void process_tickets(struct booth_config *conf)
 {
 	struct ticket_config *tk;
 	int i;
 	timetype last_cron;
 
-	_FOREACH_TICKET(i, tk) {
+	FOREACH_TICKET(conf, i, tk) {
 		if (!has_extprog_exited(tk) &&
-				is_time_set(&tk->next_cron) && !is_past(&tk->next_cron))
+				is_time_set(&tk->next_cron) && !is_past(&tk->next_cron)) {
 			continue;
+		}
 
 		tk_log_debug("ticket cron");
 
 		copy_time(&tk->next_cron, &last_cron);
-		ticket_cron(tk);
+		ticket_cron(conf, tk);
 		if (time_cmp(&last_cron, &tk->next_cron, ==)) {
 			tk_log_debug("nobody set ticket wakeup");
 			set_ticket_wakeup(tk);
@@ -1170,13 +1177,13 @@ void process_tickets(void)
 
 
 
-void tickets_log_info(void)
+void tickets_log_info(struct booth_config *conf)
 {
 	struct ticket_config *tk;
 	int i;
 	time_t ts;
 
-	_FOREACH_TICKET(i, tk) {
+	FOREACH_TICKET(conf, i, tk) {
 		ts = wall_ts(&tk->term_expires);
 		tk_log_info("state '%s' "
 				"term %d "
diff --git a/src/ticket.h b/src/ticket.h
index 4125d68..7cab1e1 100644
--- a/src/ticket.h
+++ b/src/ticket.h
@@ -122,7 +122,6 @@ int check_ticket(struct booth_config *conf, char *ticket, struct ticket_config *
 
 int grant_ticket(struct ticket_config *ticket);
 int revoke_ticket(struct ticket_config *ticket);
-int list_ticket(char **pdata, unsigned int *len);
 
 /**
  * @internal
@@ -173,7 +172,16 @@ int postpone_ticket_processing(struct ticket_config *tk);
 
 int acquire_ticket(struct ticket_config *tk, cmd_reason_t reason);
 
-int ticket_answer_list(int fd);
+/**
+ * @internal
+ * Implementation of ticket listing
+ *
+ * @param[in,out] conf config object to refer to
+ * @param[in]     fd   file descriptor of the socket to respond to
+ *
+ * @return see @list_ticket and @send_header_plus
+ */
+int ticket_answer_list(struct booth_config *conf, int fd);
 
 /**
  * @internal
@@ -190,8 +198,22 @@ int process_client_request(struct booth_config *conf, struct client *req_client,
 
 int ticket_write(struct ticket_config *tk);
 
-void process_tickets(void);
-void tickets_log_info(void);
+/**
+ * @internal
+ * Mainloop of booth ticket handling
+ *
+ * @param[in,out] conf config object to refer to
+ */
+void process_tickets(struct booth_config *conf);
+
+/**
+ * @internal
+ * Log properties of all tickets
+ *
+ * @param[in,out] conf config object to refer to
+ */
+void tickets_log_info(struct booth_config *conf);
+
 char *state_to_string(uint32_t state_ho);
 int send_reject(struct booth_site *dest, struct ticket_config *tk,
 	cmd_result_t code, struct boothc_ticket_msg *in_msg);
diff --git a/src/transport.c b/src/transport.c
index e19dcb0..49217f6 100644
--- a/src/transport.c
+++ b/src/transport.c
@@ -465,7 +465,7 @@ static void process_connection(struct booth_config *conf, int ci)
 	 * result a second later? */
 	switch (ntohl(header->cmd)) {
 	case CMD_LIST:
-		ticket_answer_list(req_cl->fd);
+		ticket_answer_list(conf, req_cl->fd);
 		goto kill;
 	case CMD_PEERS:
 		list_peers(req_cl->fd);
-- 
2.25.1

