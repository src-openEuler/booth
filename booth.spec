# RPMs are split as follows:
# * booth:
#   - envelope package serving as a syntactic shortcut to install
#     booth-site (with architecture reliably preserved)
# * booth-core:
#   - package serving as a base for booth-{arbitrator,site},
#     carrying also basic documentation, license, etc.
# * booth-arbitrator:
#   - package to be installed at a machine accessible within HA cluster(s),
#     but not (necessarily) a member of any, hence no dependency
#     on anything from cluster stack is required
# * booth-site:
#   - package to be installed at a cluster member node
#     (requires working cluster environment to be useful)
# * booth-test:
#   - files for testing booth
#
# TODO:
# wireshark-dissector.lua currently of no use (rhbz#1259623), but if/when
# this no longer persists, add -wireshark package (akin to libvirt-wireshark)

%bcond_with html_man
%bcond_with glue
%bcond_with run_build_tests
%bcond_with include_unit_test

%global release 9

## User and group to use for nonprivileged services (should be in sync with pacemaker)
%global uname hacluster
%global gname haclient

# Disable automatic compilation of Python files in extra directories
%global _python_bytecompile_extra 0

%global github_owner ClusterLabs

%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}}
# https://fedoraproject.org/wiki/EPEL:Packaging?rd=Packaging:EPEL#The_.25license_tag
%{!?_licensedir:%global license %doc}

%global test_path   %{_datadir}/booth/tests

Name:           booth
Version:        1.2
Release:        %{release}
Summary:        Ticket Manager for Multi-site Clusters
License:        GPL-2.0-or-later
Url:            https://github.com/%{github_owner}/%{name}
Source0:        https://github.com/%{github_owner}/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz
Patch01:        backport-Fix-typo-in-wait_child.patch
Patch02:        backport-Refactor-Fix-problems-found-by-clang.patch
patch03:        backport-Refactor-foreach_-node-ticket-apply-more-universally.patch
patch04:        backport-Refactor-rename-foreach_-node-ticket-macros-to-upper.patch
patch05:        backport-rpm-Add-gcc-as-a-build-dependency.patch
patch06:        backport-rpm-Remove-the-tar-file-as-part-of-make-clean.patch
patch07:        backport-build-Add-support-for-building-with-mock.patch
patch08:        backport-Refactor-Remove-the-unused-check_site-function.patch
patch09:        backport-Refactor-Remove-global-booth_conf-variable-in-read_c.patch
patch10:        backport-Refactor-Remove-global-booth_conf-variable-in-check_.patch
patch11:        backport-Refactor-Rename-FOREACH_-NODE-TICKET.patch
patch12:        backport-Refactor-Add-three-argument-versions-of-FOREACH_-NOD.patch
patch13:        backport-Refactor-Remove-global-booth_conf-variable-in-find_name.patch
patch14:        backport-Refactor-Remove-global-booth_conf-variable-in-find_id.patch
patch15:        backport-Refactor-Remove-global-booth_conf-variable-in-find_myself.patch
patch16:        backport-Refactor-Add-a-function-to-convert-a-BOOTH_DAEMON_ST.patch
patch17:        backport-Refactor-Add-a-function-to-get-the-port-from-a-booth.patch
patch18:        backport-Refactor-Use-asprintf-instead-of-a-static-buffer-in-.patch
patch19:        backport-Build-Remove-build-time-support-for-running-splint.patch
patch20:        backport-Build-Remove-unneeded-cruft-from-configure.ac.patch
patch21:        backport-Refactor-Remove-global-booth_conf-variable-in-find_t.patch
patch22:     	backport-Refactor-number_sites_marked_as_granted-is-now-a-sta.patch
Patch23:	    backport-Refactor-Remove-global-booth_conf-variable-in-ticket.patch
Patch24:        backport-Refactor-Mark-internal-functions-in-ticket.c-as-stat.patch

# direct build process dependencies
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  coreutils
BuildRequires:  make
## ./autogen.sh
BuildRequires:  /bin/sh
# general build dependencies
BuildRequires:  asciidoctor
BuildRequires:  gcc
BuildRequires:  pkgconfig
# linking dependencies
BuildRequires:  gnutls-devel
BuildRequires:  libxml2-devel
## just for <pacemaker/crm/services.h> include
BuildRequires:  pacemaker-libs-devel
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  zlib-devel
## logging provider
BuildRequires:  pkgconfig(libqb)
## random2range provider
BuildRequires:  pkgconfig(glib-2.0)
## nametag provider
BuildRequires:  pkgconfig(libsystemd)
# check scriptlet (for hostname and killall respectively)
BuildRequires:  hostname psmisc
BuildRequires:  python3-devel
# For generating tests
BuildRequires:  sed
# spec file specifics
## for _unitdir, systemd_requires and specific scriptlet macros
BuildRequires:  systemd
## for autosetup
BuildRequires:  git
%if 0%{?with_run_build_tests}
# check scriptlet (for perl and ss)
BuildRequires:  perl-interpreter iproute
%endif

# this is for a composite-requiring-its-components arranged
# as an empty package (empty files section) requiring subpackages
# (_isa so as to preserve the architecture)
Requires:       %{name}-core
Requires:       %{name}-site
%files
%license COPYING
%dir %{_datadir}/pkgconfig
%{_datadir}/pkgconfig/booth.pc

%description
Booth manages tickets which authorize cluster sites located
in geographically dispersed locations to run resources.
It facilitates support of geographically distributed
clustering in Pacemaker.

# SUBPACKAGES #

%package        core
Summary:        Booth core files (executables, etc.)
# for booth-keygen (chown, dd)
Requires:       coreutils
# deal with pre-split arrangement
Conflicts:      %{name} < 1.0-1

%description    core
Core files (executables, etc.) for Booth, ticket manager for
multi-site clusters.

%package        arbitrator
Summary:        Booth support for running as an arbitrator
BuildArch:      noarch
Requires:       %{name}-core = %{version}-%{release}
%{?systemd_requires}
# deal with pre-split arrangement
Conflicts:      %{name} < 1.0-1

%description    arbitrator
Support for running Booth, ticket manager for multi-site clusters,
as an arbitrator.

%post arbitrator
%systemd_post booth-arbitrator.service

%preun arbitrator
%systemd_preun booth-arbitrator.service

%postun arbitrator
%systemd_postun_with_restart booth-arbitrator.service

%package        site
Summary:        Booth support for running as a full-fledged site
BuildArch:      noarch
Requires:       %{name}-core = %{version}-%{release}
# for crm_{resource,simulate,ticket} utilities
Requires:       pacemaker >= 1.1.8
# for ocf-shellfuncs and other parts of OCF shell-based environment
Requires:       resource-agents
# deal with pre-split arrangement
Conflicts:      %{name} < 1.0-1

%description    site
Support for running Booth, ticket manager for multi-site clusters,
as a full-fledged site.

%package        test
Summary:        Test scripts for Booth
BuildArch:      noarch
# runtests.py suite (for hostname and killall respectively)
Requires:       hostname psmisc
# any of the following internal dependencies will pull -core package
## for booth@booth.service
Requires:       %{name}-arbitrator = %{version}-%{release}
## for booth-site and service-runnable scripts
## (and /usr/lib/ocf/resource.d/booth)
Requires:       %{name}-site = %{version}-%{release}
Requires:       gdb
Requires:       %{__python3}
%if 0%{?with_include_unit_test}
Requires:       python3-pexpect
%endif
# runtests.py suite (for perl and ss)
Requires:       perl-interpreter iproute

%description    test
Automated tests for running Booth, ticket manager for multi-site clusters.

# BUILD #

%prep
%autosetup -n %{name}-%{version} -p 1

%build
./autogen.sh
%{configure} \
        --with-initddir=%{_initrddir} \
        --docdir=%{_pkgdocdir} \
        --enable-user-flags \
        %{?with_html_man:--with-html_man} \
        %{!?with_glue:--without-glue} \
        PYTHON=%{__python3}
%{make_build}

%install
%{make_install}
mkdir -p %{buildroot}/%{_unitdir}
cp -a -t %{buildroot}/%{_unitdir} \
        -- conf/booth@.service conf/booth-arbitrator.service
install -D -m 644 -t %{buildroot}/%{_mandir}/man8 \
        -- docs/boothd.8
ln -s boothd.8 %{buildroot}/%{_mandir}/man8/booth.8
cp -a -t %{buildroot}/%{_pkgdocdir} \
        -- ChangeLog README-testing conf/booth.conf.example
# drop what we don't package anyway (COPYING added via tarball-relative path)
rm -rf %{buildroot}/%{_initrddir}/booth-arbitrator
rm -rf %{buildroot}/%{_pkgdocdir}/README.upgrade-from-v0.1
rm -rf %{buildroot}/%{_pkgdocdir}/COPYING
# tests
mkdir -p %{buildroot}/%{test_path}
# Copy tests from tarball
cp -a -t %{buildroot}/%{test_path} \
        -- conf test
%if 0%{?with_include_unit_test}
cp -a -t %{buildroot}/%{test_path} \
        -- unit-tests script/unit-test.py
%endif
chmod +x %{buildroot}/%{test_path}/test/booth_path
chmod +x %{buildroot}/%{test_path}/test/live_test.sh
mkdir -p %{buildroot}/%{test_path}/src
ln -s -t %{buildroot}/%{test_path}/src \
        -- %{_sbindir}/boothd
# Generate runtests.py and boothtestenv.py
sed -e 's#PYTHON_SHEBANG#%{__python3} -Es#g' \
    -e 's#TEST_SRC_DIR#%{test_path}/test#g' \
    -e 's#TEST_BUILD_DIR#%{test_path}/test#g' \
    %{buildroot}/%{test_path}/test/runtests.py.in > %{buildroot}/%{test_path}/test/runtests.py

chmod +x %{buildroot}/%{test_path}/test/runtests.py

sed -e 's#PYTHON_SHEBANG#%{__python3} -Es#g' \
    -e 's#TEST_SRC_DIR#%{test_path}/test#g' \
    -e 's#TEST_BUILD_DIR#%{test_path}/test#g' \
    %{buildroot}/%{test_path}/test/boothtestenv.py.in > %{buildroot}/%{test_path}/test/boothtestenv.py

# https://fedoraproject.org/wiki/Packaging:Python_Appendix#Manual_byte_compilation
%py_byte_compile %{__python3} %{buildroot}/%{test_path}

%check
# alternatively: test/runtests.py
#%if 0%{?with_run_build_tests}
VERBOSE=1 make check
#%endif

%files          core
%license COPYING
%doc %{_pkgdocdir}/AUTHORS
%doc %{_pkgdocdir}/ChangeLog
%doc %{_pkgdocdir}/README
%doc %{_pkgdocdir}/booth.conf.example
# core command(s) + man pages
%{_sbindir}/booth*
%{_mandir}/man8/booth*.8*
# configuration
%dir %{_sysconfdir}/booth
%exclude %{_sysconfdir}/booth/booth.conf.example

%dir %attr (755, root, root) %{_var}/lib/booth/
%dir %attr (755, root, root) %{_var}/lib/booth/cores

# Generated html docs
%if 0%{?with_html_man}
%{_pkgdocdir}/booth-keygen.8.html
%{_pkgdocdir}/boothd.8.html
%endif

%files          arbitrator
%{_unitdir}/booth@.service
%{_unitdir}/booth-arbitrator.service

%files          site
# OCF (agent + a helper)
## /usr/lib/ocf/resource.d/pacemaker provided by pacemaker
%{_usr}/lib/ocf/resource.d/pacemaker/booth-site
%dir %{_usr}/lib/ocf/lib/booth
     %{_usr}/lib/ocf/lib/booth/geo_attr.sh
# geostore (command + OCF agent)
%{_sbindir}/geostore
%{_mandir}/man8/geostore.8*
## /usr/lib/ocf/resource.d provided by resource-agents
%dir %{_usr}/lib/ocf/resource.d/booth
     %{_usr}/lib/ocf/resource.d/booth/geostore
# helper (possibly used in the configuration hook)
%dir %{_datadir}/booth
     %{_datadir}/booth/service-runnable

# Generated html docs
%if 0%{?with_html_man}
%{_pkgdocdir}/geostore.8.html
%endif

%files          test
%doc %{_pkgdocdir}/README-testing
# /usr/share/booth provided by -site
%{test_path}
# /usr/lib/ocf/resource.d/booth provided by -site
%{_usr}/lib/ocf/resource.d/booth/sharedrsc

%changelog
* Tue Dec 03 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-9
- Refactor: Remove global booth_conf variable in ticket.c.
- Refactor: Mark internal functions in ticket.c as static.

* Tue Nov 05 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-8
- Refactor: Remove global booth_conf variable in find_ticket_by_name
- Refactor: number_sites_marked_as_granted is now a static function.
-
* Tue Oct 29 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-7
- Build: Remove build-time support for running splint.
- Build: Remove unneeded cruft from configure.ac

* Thu Oct 24 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-6
- Apply setup_udp_server refactorings

* Thu Oct 17 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-5
- Apply find_* function refactorings

* Thu Sep 19 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-4
- Apply config.c refactorings

* Mon Aug 12 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-3
- Add support for building with mock
- delete "-S git" from %autosetup

* Fri Aug 09 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-2
- Fix typo in wait_child 
- Refactor: foreach_{node,ticket}: apply more universally
- Refactor: rename foreach_{node,ticket} macros to uppercased variants
- Refactor Fix problems found by clang

* Wed Jun 19 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.2-1
- Update version to 1.2

* Thu Jun 13 2024 bizhiyuan <bizhiyuan@kylinos.cn> -1.1-8
- pacemaker store booth cfg name attribute
- transport fix find_myself for kernel 6.9 

* Tue Jun 11 2024 zouzhimin <zouzhimin@kylinos.cn> -1.1-7
- Add support for GnuTLS as and alternative for mcrypt and gcrypt

* Sun Jun 09 2024 xuchenchen <xuchenchen@kylinos.cn> -1.1-6
- Type:CVES
- ID:CVE-2024-3049
- SUG:NA
- DESC:fix CVE-2024-3049

* Sun Apr 28 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1-5
- pacemaker Check snprintf return values

* Thu Feb 29 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1-4
- pacemaker: Use long format for crm_ticket -v

* Thu Feb 29 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1-3
- pacemaker: Don't add explicit error prefix in log

* Tue Feb 20 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1-2
- pacemaker: Remove const warning

* Fri Nov 17 2023 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1-1
- Update to version 1.1 

* Tue Oct 17 2023 bizhiyuan <bizhiyuan@kylinos.cn> - 1.0-9
- Fix glib hash table is not NULL assert

* Fri Mar 17 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-8
- Modify folder permissions

* Tue Feb 28 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-7
- Delete the error macro

* Tue Feb 07 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-6
- Rebase to newest upstream snapshot

* Wed Mar 16 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-5
- Package /var/lib/booth where booth can chroot

* Mon Feb 28 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-4
- Fix booth@.service missing instance name.

* Fri Oct 30 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0-3
- Init booth project
