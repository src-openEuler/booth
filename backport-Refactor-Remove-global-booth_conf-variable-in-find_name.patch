From a6bbee968ec9a2a4c11b7f1a90b8a21dfc07d2e7 Mon Sep 17 00:00:00 2001
From: Chris Lumens <clumens@redhat.com>
Date: Thu, 11 Jul 2024 10:45:35 -0400
Subject: [PATCH 3/5] Refactor: Remove global booth_conf variable in
 find_site_by_name...
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

...as well as functions that call it.  Also, change the site argument to
be const.

Co-authored-by: Jan Pokorný <jpokorny@redhat.com>
---
 src/attr.c   |  4 ++--
 src/attr.h   | 13 ++++++++++++-
 src/config.c | 21 +++++++++++++--------
 src/config.h | 15 ++++++++++++++-
 src/main.c   |  8 ++++----
 5 files changed, 45 insertions(+), 16 deletions(-)

diff --git a/src/attr.c b/src/attr.c
index daa7648..086dc98 100644
--- a/src/attr.c
+++ b/src/attr.c
@@ -149,7 +149,7 @@ static int read_server_reply(
 	return rv;
 }
 
-int do_attr_command(cmd_request_t cmd)
+int do_attr_command(struct booth_config *conf, cmd_request_t cmd)
 {
 	struct booth_site *site = NULL;
 	struct boothc_header *header;
@@ -160,7 +160,7 @@ int do_attr_command(cmd_request_t cmd)
 	if (!*cl.site)
 		site = local;
 	else {
-		if (!find_site_by_name(cl.site, &site, 1)) {
+		if (!find_site_by_name(conf, cl.site, &site, 1)) {
 			log_error("Site \"%s\" not configured.", cl.site);
 			goto out_close;
 		}
diff --git a/src/attr.h b/src/attr.h
index 1c680bd..547d19a 100644
--- a/src/attr.h
+++ b/src/attr.h
@@ -31,7 +31,18 @@
 
 void print_geostore_usage(void);
 int test_attr_reply(cmd_result_t reply_code, cmd_request_t cmd);
-int do_attr_command(cmd_request_t cmd);
+
+/**
+ * @internal
+ * Carry out a geo-atribute related command
+ *
+ * @param[in,out] conf  config object to refer to
+ * @param[in]     cmd   what to perform
+ *
+ * @return 0 or negative value (-1 or -errno) on error
+ */
+int do_attr_command(struct booth_config *conf, cmd_request_t cmd);
+
 int process_attr_request(struct client *req_client, void *buf);
 int attr_recv(void *buf, struct booth_site *source);
 int store_geo_attr(struct ticket_config *tk, const char *name, const char *val, int notime);
diff --git a/src/config.c b/src/config.c
index 5e34919..5236505 100644
--- a/src/config.c
+++ b/src/config.c
@@ -991,16 +991,18 @@ g_inval:
 }
 
 
-static int get_other_site(struct booth_site **node)
+static int get_other_site(struct booth_config *conf,
+			  struct booth_site **node)
 {
 	struct booth_site *n;
 	int i;
 
 	*node = NULL;
-	if (!booth_conf)
+	if (conf == NULL) {
 		return 0;
+	}
 
-	_FOREACH_NODE(i, n) {
+	FOREACH_NODE(conf, i, n) {
 		if (n != local && n->type == SITE) {
 			if (!*node) {
 				*node = n;
@@ -1014,18 +1016,21 @@ static int get_other_site(struct booth_site **node)
 }
 
 
-int find_site_by_name(char *site, struct booth_site **node, int any_type)
+int find_site_by_name(struct booth_config *conf, const char *site,
+		      struct booth_site **node, int any_type)
 {
 	struct booth_site *n;
 	int i;
 
-	if (!booth_conf)
+	if (conf == NULL) {
 		return 0;
+	}
 
-	if (!strcmp(site, OTHER_SITE))
-		return get_other_site(node);
+	if (!strcmp(site, OTHER_SITE)) {
+		return get_other_site(conf, node);
+	}
 
-	_FOREACH_NODE(i, n) {
+	FOREACH_NODE(conf, i, n) {
 		if ((n->type == SITE || any_type) &&
 		    strncmp(n->addr_string, site, sizeof(n->addr_string)) == 0) {
 			*node = n;
diff --git a/src/config.h b/src/config.h
index ad7aed2..1a7f40c 100644
--- a/src/config.h
+++ b/src/config.h
@@ -355,7 +355,20 @@ int read_config(struct booth_config **conf, const char *path, int type);
  */
 int check_config(struct booth_config *conf, int type);
 
-int find_site_by_name(char *site, struct booth_site **node, int any_type);
+/**
+ * @internal
+ * Find site in booth configuration by resolved host name
+ *
+ * @param[in,out] conf config object to refer to
+ * @param[in] site name to match against previously resolved host names
+ * @param[out] node relevant tracked data when found
+ * @param[in] any_type whether or not to consider also non-site members
+ *
+ * @return 0 if nothing found, or 1 when found (node assigned accordingly)
+ */
+int find_site_by_name(struct booth_config *conf, const char *site,
+		      struct booth_site **node, int any_type);
+
 int find_site_by_id(uint32_t site_id, struct booth_site **node);
 
 const char *type_to_string(int type);
diff --git a/src/main.c b/src/main.c
index 48736b5..445655c 100644
--- a/src/main.c
+++ b/src/main.c
@@ -394,7 +394,7 @@ static int setup_config(struct booth_config **conf, int type)
 
 	/* Set "local" pointer, ignoring errors. */
 	if (cl.type == DAEMON && cl.site[0]) {
-		if (!find_site_by_name(cl.site, &local, 1)) {
+		if (!find_site_by_name(booth_conf, cl.site, &local, 1)) {
 			log_error("Cannot find \"%s\" in the configuration.",
 					cl.site);
 			return -EINVAL;
@@ -708,7 +708,7 @@ static int query_get_string_answer(cmd_request_t cmd)
 
 	if (!*cl.site)
 		site = local;
-	else if (!find_site_by_name(cl.site, &site, 1)) {
+	else if (!find_site_by_name(booth_conf, cl.site, &site, 1)) {
 		log_error("cannot find site \"%s\"", cl.site);
 		rv = ENOENT;
 		goto out;
@@ -782,7 +782,7 @@ static int do_command(cmd_request_t cmd)
 	if (!*cl.site)
 		site = local;
 	else {
-		if (!find_site_by_name(cl.site, &site, 1)) {
+		if (!find_site_by_name(booth_conf, cl.site, &site, 1)) {
 			log_error("Site \"%s\" not configured.", cl.site);
 			goto out_close;
 		}
@@ -1617,7 +1617,7 @@ static int do_attr(struct booth_config **conf)
 
 	case ATTR_SET:
 	case ATTR_DEL:
-		rv = do_attr_command(cl.op);
+		rv = do_attr_command(booth_conf, cl.op);
 		break;
 	}
 
-- 
2.25.1

